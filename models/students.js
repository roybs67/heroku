'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Students extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Students.hasMany(models.Subjects)
      // Students.hasMany(models.StudentAll)
      Students.belongsToMany(models.Entrance, {
        through: models.StudentAll
      })
    }
  };
  Students.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    NIM: DataTypes.STRING,
    div: DataTypes.STRING,
    Password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Students',
  });
  return Students;
};