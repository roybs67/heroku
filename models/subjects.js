'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Subjects extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Subjects.belongsTo(models.Students, {foreignKey: "StudentId"})
    }
  }
  Subjects.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    StudentId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Subjects',
  });
  return Subjects;
};