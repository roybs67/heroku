'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentAll extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // StudentAll.belongsTo(models.Students)
      // StudentAll.belongsTo(models.Entrance)
      //models.Students.belongsToMany(models.Entrance, { through: models.StudentAll})
      //models.Entrance.belongsToMany(models.Students, { through: models.StudentAll})
    }
  }
  StudentAll.init({
    StudentId: DataTypes.STRING,
    EntranceId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'StudentAll',
  });
  return StudentAll;
};