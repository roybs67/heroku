'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Entrance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Entrance.belongsToMany(models.Students, {
        through: models.StudentAll
      })
      // Entrance.hasMany(models.StudentAll)
    }
  }
  Entrance.init({
    Name: DataTypes.STRING,
    Year: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Entrance',
  });
  return Entrance;
};