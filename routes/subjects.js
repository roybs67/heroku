const { SubjectController } = require("../controller")
const route = require("express").Router()


route.get("/", SubjectController.getSubjects)
route.get("/:id", SubjectController.getSubject)
route.post("/", SubjectController.createSubject)
route.put("/:id", SubjectController.updateSubject)
route.delete("/:id", SubjectController.deleteSubject)


module.exports = route
