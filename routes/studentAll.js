const { StudentAllController } = require("../controller")
const route = require("express").Router()
const { authentication} = require("../Middleware")

route.get("/", StudentAllController.getStudentAlls)
route.get("/:id", StudentAllController.getStudentAll)
route.use(authentication)
route.post("/", StudentAllController.createStudentAll)
route.put("/:id", StudentAllController.updateStudentAll)
route.delete("/:id", StudentAllController.deleteStudentAll)


module.exports = route
