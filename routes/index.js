const studentRoute = require("./student")
const subjectRoute = require("./subjects")
const entranceRoute = require("./entrance")
const studentAllRoute = require("./studentAll")
const route = require("express").Router()


route.use("/student", studentRoute)
route.use("/subject", subjectRoute)
route.use("/entrance", entranceRoute)
route.use("/studentall", studentAllRoute)


module.exports = route
