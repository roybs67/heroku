const { EntranceController } = require("../controller")
const route = require("express").Router()


route.get("/", EntranceController.getEntrances)
route.get("/:id", EntranceController.getEntrance)
route.post("/", EntranceController.createEntrance)
route.put("/:id", EntranceController.updateEntrance)
route.delete("/:id", EntranceController.deleteEntrance)


module.exports = route
