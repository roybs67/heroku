const {  Students, Entrance } = require("../models")

class EntranceController {
  static async getEntrances(req, res) {
    try {
      const options = {
        include: [
          {
            model:Students ,
            // attributes : ["name"]
          }
        ]
      }
      const data = await Entrance.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getEntrance(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Entrance.findOne(options)

    res.status(200).json({ data })
  }

  static async updateEntrance(req, res) {
    const { Name, Year} = req.body

    const payload = {
        Name, Year
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Entrance.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createEntrance(req, res) {
    const { Name, Year} = req.body
    const payload = {
        Name, Year
    }

    const newEntrance = await Entrance.create(payload)
    res.status(200).json({ data: newEntrance })
  }


  static async deleteEntrance(req, res) {
    const id = req.params.id
    const deleted = await Entrance.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = EntranceController
