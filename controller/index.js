const StudentController = require("./student.controller")
const SubjectController = require("./subjects.controller")
const EntranceController = require("./entrance.controller")
const StudentAllController = require("./StudentAll.controller")

module.exports = {
  StudentController,
  SubjectController,
  EntranceController,
  StudentAllController
}
