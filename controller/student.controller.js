const { Students, Subjects, Entrance} = require("../models")
const uuid = require("uuid")
const { token, encrypt } = require("../utils")

class StudentController {

  static async login(req, res) {
    const { name, Password } = req.body
    const student = await Students.findOne({
      where: {
        name
      }
    })
    const compare = student ? encrypt.isPwdValid(Password, student.dataValues.Password) : 0
    console.log(student)
    // const checkPassword = student ? student.dataValues.Password === encryptPass : 0
    if (!student || !compare) {
      res.status(401).json({
        status: 401,
        message: "Unauthorize"
      })
      return
    }
    const access_token = token.makeToken(student.dataValues)
    res.status(200).json({ access_token })



  }



  static async getStudents(req, res) {
    try {
      const options = {
        include: [
          {
            model:Subjects ,
            attributes : ["name"]
          },
          {
            model:Entrance ,
            // attributes : ["Name"]
          }
        ]
      }
      const data = await Students.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getStudent(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
      include: [
        {
          model:Subjects
        }
      ]
    }
    const data = await Students.findOne(options)

    res.status(200).json({ data })
  }

  static async updateStudent(req, res) {
    const { name, NIM, div} = req.body

    const payload = {
      name, NIM, div
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Students.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createStudent(req, res) {
    const id = uuid.v4()
    let { name, NIM, div, Password} = req.body
    Password = encrypt.encryptPwd(Password)
    const payload = {
        name, NIM, div, id, Password
    }

    const newStudent = await Students.create(payload)
    res.status(200).json({ data: newStudent })
  }


  static async deleteStudent(req, res) {
    const id = req.params.id
    const deleted = await Students.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = StudentController
