const { Subjects, Students} = require("../models")
const uuid = require("uuid")

class SubjectController {
  static async getSubjects(req, res) {
    try {
      const options = {
        include: [
          {
            model: Students,
            attributes: ["name"]
          }
        ]
      }

      const data = await Subjects.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getSubject(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Subjects.findOne(options)

    res.status(200).json({ data })
  }

  static async updateSubject(req, res) {
    const { name, StudentId} = req.body

    const payload = {
        name, StudentId
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Subjects.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createSubject(req, res) {
    const id = uuid.v4()
    const { name, StudentId} = req.body
    const payload = {
        name, StudentId, id
    }

    const newSubjects = await Subjects.create(payload)
    res.status(200).json({ data: newSubjects })
  }


  static async deleteSubject(req, res) {
    const id = req.params.id
    const deleted = await Subjects.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = SubjectController
