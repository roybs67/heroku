const {  Students, Entrance, StudentAll } = require("../models")

class StudentAllController {
  static async getStudentAlls(req, res) {
    try {
      const options = {
        include: [
          {
            model:Students ,
            attributes : ["name"]
          },
          {
            model: Entrance,
            attributes: ["Name"]
          }
        ]
      }
      const data = await StudentAll.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getStudentAll(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }
    const data = await StudentAll.findOne(options)

    res.status(200).json({ data })
  }

  static async updateStudentAll(req, res) {
    const { StudentId, EntranceId} = req.body

    const payload = {
        StudentId, EntranceId
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await StudentAll.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createStudentAll(req, res) {
    const { StudentId, EntranceId} = req.body
    const payload = {
        StudentId, EntranceId
    }

    const newStudentAll = await StudentAll.create(payload)
    res.status(200).json({ data: newStudentAll })
  }


  static async deleteStudentAll(req, res) {
    const id = req.params.id
    const deleted = await StudentAll.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = StudentAllController
